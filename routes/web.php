<?php

use App\Http\Controllers\CatalogController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NobitaController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('auth/login', function () {
    return view('auth/login');
    
    
    /*return response('Respuesta', 200)
                  ->header('Content-Type', 'text/plain');
    
    return response('Error', 404)
                  ->header('Content-Type', 'text/plain');*/
});

Route::get('auth/logout', function () {
    return'Logout usuario';
});
 

 Route::get('catalog/delete/{id}', function ($id) {
    return view('catalog/delete',compact('id'));
});


Route::get('/', [HomeController::class, 'getHome']); 

Route::get('catalog', [CatalogController::class, 'getIndex']); 

 

Route::resource('/nobi', NobitaController::class);

//Route::view('/', 'catalog/index')/*->middleware('language')*/;

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Auth::routes(['verify' => 'true']);

Route::group(['middleware' => 'verified'], function () {


    Route::get('catalog/create', [CatalogController::class, 'getCreate']); 

    Route::get('catalog/edit/{id}', [CatalogController::class, 'getEdit']); 
    
    Route::get('catalog/show/{id}', [CatalogController::class, 'getShow']);

    
   Route::put('catalog/edit/{id}',[CatalogController::class,'putEdit']);

});

Route::post('catalog/create',[CatalogController::class,'postCreate']);

Route::delete('/catalog/delete/{id}',[CatalogController::class,'putDelete']);
























/*
Route::get('/', function () {
    return view('home');
});
*/

/*
Route::get('catalog', function () {
    return view('catalog/index');
});

Route::get('catalog/show/{id}', function ($id) {
    return view('catalog/show',compact('id'));
});

Route::get('catalog/create', function () {
    return view('catalog/create');
});

Route::get('catalog/edit/{id}', function ($id) {
    return view('catalog/edit',compact('id'));
});
*/

/*
Route::get('host', function () {
    return env('DB_HOST');
});

Route::get('timezone', function () {
    return config('app.timezone');
});

*/

// A32 --------------------------------------------------------------------
// 3.1
/*
Route::get('catalog/show/{id?}', function ($id = null) {
    return $id;
});
// 3.2
Route::get('catalog/show/{id?}', function ($id = 1) {
    return $id;
});
*/
// 3.3
/*
Route::post('catalog/post/create', function () {
    return 'Crea catalogo con post';
});
// 3.4

Route::match(['get', 'post'], '/', function () {
    //
});
// 3.5
Route::get('catalog/show/{id}', function ($id) {
    //
})->where('id', '[0-9]+');
// 3.6
Route::get('catalog/show/{id}/{name}', function ($id, $name) {
    //
})->where([,'name' => '[a-z]+','id' => '[0-9]+']);
//-------------------------------------------------------------------------
*/

// A33 --------------------------------------------------------------------

/*
Route::get('/welcome', function () {
    return view('welcome');
});

// 1.1
Route::get('/inicio', function () {
    return view('home');
});
// 1.2
Route::get('/fecha', function () {
    return view('fecha');
});

*/


    /*
//1.2

 Route::get('/fecha/{day?}/{month?}/{year?}',function($day = 25,$month = 10,$year = 1999){
      return view('fecha', ['day' => $day, 'month' => $month, 'year' => $year]);
 });
 //1.3

 Route::get('/fecha/{day?}/{month?}/{year?}',function($day = 25,$month = 10,$year = 1999){
     $fecha = array('day','month','year');
    return view('fecha', compact($fecha));
 });
 //1.4

Route::get('/fecha/{day?}/{month?}/{year?}',function($day = null,$month = null,$year = null){
     $callback = function ($valor){
        return (is_null($valor)) ? " 00 " : $valor;
 };
    return view('fecha', ['day' => with($day, $callback), 'month' => with($month,$callback), 'year' => with($year,$callback)]);
});
      */
//-------------------------------------------------------------------------

/*
Route::get('/images', function () {
    return view('images');
});
*/
//A 34
//-------------------------------------------------------------------------
/*
Route::get('/master', function () {
    return view('/layouts/master');
});
*/

//A 35
//-------------------------------------------------------------------------
/*
Route::get('catalog/show/{id}', [CatalogController::class, 'getShow']);

Route::get('/', [HomeController::class, 'getHome']); 

Route::get('catalog', [CatalogController::class, 'getIndex']); 

Route::get('catalog/create', [CatalogController::class, 'getCreate']); 

Route::get('catalog/edit/{id}', [CatalogController::class, 'getEdit']); 

Route::resource('/nobi', [NobitaController::class]); 
*/
