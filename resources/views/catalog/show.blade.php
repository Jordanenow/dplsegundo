@extends('layouts.master')

@section('content')

    <h1>Muestra {{$cliente -> id}}</h1>

    <div class="container">
    <div class="row">



<div class="col-sm-4">


<p>{{$cliente -> imagen}}</p>


</div>

<div class="col-sm-8">


<p>{{$cliente -> nombre}}</p>
<p>{{$cliente -> fecha_nacimiento}}</p>
<p>{{$cliente -> correo}}</p>

<a class="btn btn-warning" href= "{{url('catalog/edit/'.$cliente->id)}}" >Editar el perfil</a>
<form method="POST" action="{{url('/catalog/delete').'/'.$cliente->id}}" style="display:inline">

 @method('DELETE')

 @csrf

 <button type="submit" class="btn btn-danger" role="button">

 Borrar

 </button>

</form>
</div>


</div>
    </div>

@stop