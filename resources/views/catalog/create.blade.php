@extends('layouts.master')

@section('content')

<P>Crear</P>

<div class="row" style="margin-top:40px">
   <div class="offset-md-3 col-md-6">
      <div class="card">
         <div class="card-header text-center">
            Añadir cliente
         </div>
         <div class="card-body" style="padding:30px">

            {{-- TODO: Abrir el formulario e indicar el método POST --}}
            <form action="" method="post" enctype="multipart/form-data">
            {{-- TODO: Protección contra CSRF --}}
            @csrf
            <div class="form-group">
            @method('POST')
            <label for="nombre">Nombre</label>
               <input type="text" name="nombre" id="nombre" class="form-control" value="">
            </div>

            <div class="form-group">
               {{-- TODO: Completa el input para la imagen --}}
               <label for="imagen">Imagen</label>
               <input type="file" name="imagen" id="imagen" class="form-control" value="">
            
            </div>

            <div class="form-group">
               {{-- TODO: Completa el input para el fecha de nacimiento --}}
               <label for="fecha">Fecha de nacimiento</label>
               <input type="text" name="fecha" id="fecha" class="form-control" value="">
            
            </div>

            <div class="form-group">
               {{-- TODO: Completa el input para el correo --}}
               <label for="correo">Correo electronico</label>
               <input type="text" name="correo" id="correo" class="form-control" value="">
            
            </div>
           
            <div class="form-group text-center">
               <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                   Crear cliente
               </button>
            </div>
            
            </form>
            {{-- TODO: Cerrar formulario --}}

         </div>
      </div>
   </div>
</div>
@stop