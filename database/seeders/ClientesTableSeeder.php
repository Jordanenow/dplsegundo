<?php

namespace Database\Seeders;
use App\Models\Cliente;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class ClientesTableSeeder extends Seeder
{
    private $arrayClientes = array(

        array(
       
        'nombre' => 'Neo',
       
        'imagen' => 'URL de la imagen',
       
        'fecha_nacimiento' => '1972-01-06',
       
        'correo' => 'neo@neo.es'
       
        ),
       
        array(
       
        'nombre' => 'Ruby',
       
        'imagen' => 'URL de la imagen',
       
        'fecha_nacimiento' => '1997-03-05',
       
        'correo' => 'ruby@ruby.es'
       
        )
       
       );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
      
        DB::table('clientes')->delete();

        Cliente::factory()->count(50)->create();
    
   /* 
 foreach ($this->arrayClientes as $cliente) {

    $c = new Cliente;
   
    $c->nombre = $cliente['nombre'];
   
    $c->imagen = $cliente['imagen'];
   
    $c->fecha_nacimiento = $cliente['fecha_nacimiento'];
   
    $c->correo = $cliente['correo'];
   
    $c->save();
   
    }

    Cliente::create(['nombre'=>'Jesus','imagen'=>'foto1', 'fecha_nacimiento'=>'2000-01-05', 'correo'=>'arribi@arribi.es']);

    Cliente::create(['nombre'=>'Pedro','imagen'=>'foto2','fecha_nacimiento'=>'2001-01-05', 'correo'=>'pedro@arribi.es']);
    
    return  ['arrayClientes'=>$this->arrayClientes];
    */
    }
}
