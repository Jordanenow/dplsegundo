<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NobitaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return 'INdex';
        dd($request->server('HTTP_ACCEPT_LANGUAGE'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return 'create';
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->input('name');
        return 'tienda';
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return 'Muestra';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return 'Edit';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return 'Update';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return 'Destroy';
    }
}
/*
$request->path(); Con esto recuperamos la URI  actual de una solicitud(Ruta).

$request->url(); Con esto recuperamos la URI  completa de una solicitud(Dominio y ruta).

$request->input('titulo') (Tienes que pasar el título por URL para poder probarlo);
Con este añadimos información a traves de la URL.

$request->fullUrl(); Esta es la forma mas completa de obtener la URI (Dominio, ruta y parámetros de ruta)
*/