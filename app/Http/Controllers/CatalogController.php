<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Cliente;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class CatalogController extends Controller
{
    

    public function getShow($id)
    {
       // return view('catalog/show',["id"=>$id]);
        
        
        $Cliente = Cliente::findOrFail($id);
        
        return view('catalog/show', ['cliente' => $Cliente]);

        /*return view('catalog/show', [
            'cliente' => Cliente::findOrFail($id)
        ]);
*/
      
    }

    public function getIndex()
    {


        $Cliente = DB::table('clientes')->paginate(10);

        return view('catalog/index', ['clientes' => $Cliente]);
      
        /*  $Cliente = Cliente::findOrFail();
        
        return view('catalog/index', ['cliente' => $Cliente]);*/
        
      // return view('catalog/index', ['arrayClientes'=>$this->arrayClientes]);
    }

    public function getCreate()
    {
        return view('catalog/create');
    }

    public function getEdit($id)
    {
        $Cliente = Cliente::findOrFail($id);
        
        return view('catalog/edit', ['cliente' => $Cliente]);

    
        
        //return view('catalog/edit',["id"=>$id]);
       

        /*return view('catalog/edit',compact("id"));*/
    }

    public function postCreate(Request $request)
    {
        $request -> file('imagen') -> store('public');
        $cliente = new Cliente();
        $cliente -> nombre = $request -> input('nombre');
        $request->validate([

            'imagen' => 'required|mimes:png,jpg|max:2048',
           
           ]);
        $cliente -> imagen = asset('storage/'. $request->file('imagen')-> hashName());
        $cliente -> fecha_nacimiento = $request -> input('fecha');
        $cliente -> correo = $request -> input('correo');
        $cliente -> save();
        
        return redirect('catalog');
    
   
    }

    public function putEdit(Request $request,$id)
    {
      
        $cliente = Cliente::findorfail($id);
        $cliente -> nombre = $request -> input('nombre');
        $cliente -> fecha_nacimiento = $request -> input('fecha');
        $cliente -> correo = $request -> input('correo');
        $cliente -> save();
        return redirect('catalog');
 
    }

    public function putDelete(Request $request,$id)
    {
      
     $borrar =  Cliente::findorfail($id);
     $borrar -> delete();
     return redirect('catalog');
 
    }

}
